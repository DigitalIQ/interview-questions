- What you have learned recently?

- What excites or interests you about coding?
- What is a recent technical challenge you experienced and how did you solve it?
- When building a new web site or maintaining one, can you explain some techniques you have used to increase performance?
- Can you describe some `coding best practices` or techniques you have used lately?
- Can you explain any common techniques or recent issues solved in regards to front-end security?
- What actions have you personally taken on recent projects to increase maintainability of your code?
- Talk about your preferred development environment.
- Which version control systems are you familiar with?
- Can you describe your workflow when you create a web page?

- Can you describe the difference between progressive enhancement and graceful degradation?

> Progressive enhancement refers to a feature of your web site that enhances the experience for browsers that support it, but has no impact if your browser does not. As an example - consider IndexedDB. It lets you store data in a client-side database. If my web site made use of Ajax to load a large amount of data, I could use IndexedDB to cache data. If your browser doesn’t support IndexedDB, then we just do Ajax to load that data. Modern browsers get a benefit, older browsers don’t get screwed.

> On the flip side, graceful degradation takes that modern feature and acts as if it is the default, but will not completely break the older browser. So in this case, the use of IndexedDB would be considered “normal”, not the additional benefit for the modern browser.

- How would you optimize a website's assets/resources?

Some common steps are:

> I’d optimize my images. In the past, I’ve used a Grunt script for this which makes it brain dead easy. You can point to a set of images, specify an optimization level, run it, and check the output to ensure it still looks good.

> Another option for images is to use CSS sprites for related icons and the such. That reduces the network requests.

> You can use a CDN that supports knowing and responding to a user’s location to better serve up the assets.

> For text files, you can minify, both CSS and JavaScript. For CSS, you can remove unused CSS

---

- If you jumped on a project and they used tabs and you used spaces, what would you do?

- Describe how you would create a simple slideshow page.

- If you could master one technology this year, what would it be?

* Do you know what [ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA) and [Screen Readers](https://www.afb.org/blindness-and-low-vision/using-technology/assistive-technology-products/screen-readers) are?

> ARIA:

Accessible Rich Internet Applications (ARIA) is a set of attributes that define ways to make web content and web applications (especially those developed with JavaScript) more accessible to people with disabilities.

> Screen readers are software programs that allow blind or visually impaired users to read the text that is displayed on the computer screen with a speech synthesizer or braille display. A screen reader is the interface between the computer's operating system, its applications, and the user.

---

- Explain some of the pros and cons for CSS animations versus JavaScript animations.

* What does [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) stand for and what issue does it address?
  > Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell browsers to give a web application running at one origin, access to selected resources from a different origin.

---

- How did you handle a disagreement with your boss or your collaborator?

- What resources do you use to learn about the latest in front end development and design?
